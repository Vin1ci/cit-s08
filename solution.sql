-- Martensen, Ian Kenneth A.
-- Retrieve complete and complex data efficiently using advanced selects and joining tables.

-- a. Find all artists that has letter D in its name.
SELECT * FROM artists WHERE name LIKE '%D%';
-- Answer: 
-- +----+---------------+
-- | id | name          |
-- +----+---------------+
-- |  4 | Lady Gaga     |
-- |  6 | Ariana Grande |
-- +----+---------------+

-- b. Find all songs that has a length of less than 230.
SELECT * FROM songs WHERE length < 230;
-- Answer: 
-- +----+--------------+----------+----------------------------------------+----------+
-- | id | song_name    | length   | genre                                  | album_id |
-- +----+--------------+----------+----------------------------------------+----------+
-- |  3 | Pardon Me    | 00:02:23 | Rock                                   |        1 |
-- |  4 | Stellar      | 00:02:00 | Rock                                   |        1 |
-- |  6 | Love Story   | 00:02:13 | Country                                |        3 |
-- |  8 | Red          | 00:02:04 | Country                                |        4 |
-- |  9 | Black Eyes   | 00:01:51 | Rock and Roll                          |        5 |
-- | 10 | Shallow      | 00:02:01 | Country, Rock, Folk rock               |        5 |
-- | 12 | Sorry        | 00:01:32 | Dancehall-poptrophical housemoombahton |        7 |
-- | 15 | Thank U Next | 00:01:56 | Pop, R&B                               |       10 |
-- | 16 | 24K Magic    | 00:02:07 | Funk, Disco, R&B                       |       11 |
-- | 17 | Lost         | 00:01:52 | Pop                                    |       12 |
-- +----+--------------+----------+----------------------------------------+----------+

-- c. Join the 'albums' and 'songs' tables. (Only show the album name, song name, and song length.)
SELECT albums.album_title, songs.song_name, songs.length
FROM albums
JOIN songs ON albums.id = songs.id;
-- Answer: 
-- +-----------------+----------------+----------+
-- | album_title     | song_name      | length   |
-- +-----------------+----------------+----------+
-- | Make Yourself   | Gangnam Style  | 00:02:53 |
-- | Fearless        | Pardon Me      | 00:02:23 |
-- | Red             | Stellar        | 00:02:00 |
-- | A Star is Born  | Fearless       | 00:02:46 |
-- | Born This Way   | Love Story     | 00:02:13 |
-- | Purpose         | State of Grace | 00:02:43 |
-- | Believe         | Red            | 00:02:04 |
-- | Dangerous Woman | Black Eyes     | 00:01:51 |
-- | Thank U, Next   | Shallow        | 00:02:01 |
-- | 24K Magic       | Born This Way  | 00:02:52 |
-- | Earth to Mars   | Sorry          | 00:01:32 |
-- +-----------------+----------------+----------+

-- d. Join the 'artists' and 'albums' tables. (Find all albums that has letter A in its name.)
SELECT albums.album_title, artists.name
FROM albums
JOIN artists ON albums.artist_id = artists.id
WHERE albums.album_title LIKE '%A%';
-- Answer: 
-- +-----------------+---------------+
-- | album_title     | name          |
-- +-----------------+---------------+
-- | Make Yourself   | Incubus       |
-- | Fearless        | Taylor Swift  |
-- | A Star is Born  | Lady Gaga     |
-- | Born This Way   | Lady Gaga     |
-- | Dangerous Woman | Ariana Grande |
-- | Thank U, Next   | Ariana Grande |
-- | 24K Magic       | Bruno Mars    |
-- | Earth to Mars   | Bruno Mars    |
-- +-----------------+---------------+

-- e. Sort the albums in Z-A order. (Show only first 4 records.)
SELECT albums.album_title, artists.name
FROM albums
JOIN artists ON albums.artist_id = artists.id
WHERE albums.album_title LIKE '%A%'
ORDER BY albums.album_title DESC
LIMIT 4;
-- Answer: 
-- +---------------+---------------+
-- | album_title   | name          |
-- +---------------+---------------+
-- | Thank U, Next | Ariana Grande |
-- | Make Yourself | Incubus       |
-- | Fearless      | Taylor Swift  |
-- | Earth to Mars | Bruno Mars    |
-- +---------------+---------------+

-- f. Join the 'albums' and 'songs' tables. (Sort albums from Z-A and sort songs from A-Z.)
SELECT albums.album_title, songs.song_name
FROM albums
JOIN songs ON albums.id = songs.album_id
ORDER BY albums.album_title DESC, songs.song_name ASC;
-- Answer: 
-- +-----------------+----------------+
-- | album_title     | song_name      |
-- +-----------------+----------------+
-- | Thank U, Next   | Thank U Next   |
-- | Red             | Red            |
-- | Red             | State of Grace |
-- | Purpose         | Sorry          |
-- | Psy 6           | Gangnam Style  |
-- | Make Yourself   | Pardon Me      |
-- | Make Yourself   | Stellar        |
-- | Fearless        | Fearless       |
-- | Fearless        | Love Story     |
-- | Earth to Mars   | Lost           |
-- | Dangerous Woman | Into You       |
-- | Born This Way   | Born This Way  |
-- | Believe         | Boyfriend      |
-- | A Star is Born  | Black Eyes     |
-- | A Star is Born  | Shallow        |
-- | 24K Magic       | 24K Magic      |
-- +-----------------+----------------+
